import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('This is my dog, Sevan Telpian', style: TextStyle(
                    fontWeight: FontWeight.bold,)
                  ),
                ),
                Text('Her name is pronounced Seh-vah-n', style: TextStyle(
                  color: Colors.grey[500],),
                ),
              ],
            ),
          ),
          Icon(Icons.pets, color: Colors.red[500],),
          Text('77'),
        ],
      ),
    );
   Color color = Theme.of(context).primaryColor;

   Widget buttonSection = Container(
     child: Row(
       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
       children: [
         _buildButtonColumn(color, Icons.fastfood, 'Treat'),
         _buildButtonColumn(color, Icons.local_drink, 'Water'),
         _buildButtonColumn(color, Icons.alarm, 'Nap Time'),
       ],
     ),
   );
   Widget textSection = Container(
     padding: const EdgeInsets.all(32),
     child: Text(
       'This is my dog Sevan Telpian. Sevan is a French Bulldog. '
           'I got her on August 2nd, and highkey it was the best day of my life. '
           'She is 3 months old, and I am in the midst of potty training her. She '
           'is not yet trained, because at first she was eatting her puppy pads, and '
           'now she eats her poop, but apparently thats what puppies do, so I will '
           'have to deal with it for now! I love Sevan a lot though and in the less than 2 months '
           'we have had her, she has made everyone in the house super happy on a daily basis.',
       softWrap: true,
     ),
   );
   return MaterialApp(
     title: 'Homework 2: Layout',
     home: Scaffold(
       appBar: AppBar(
         title: Text('Homework 2: Layout'),
       ),
       body: ListView(
         children: [
           Image.asset(
             'images/sevan1.jpg',
             width: 600,
             height: 240,
             fit: BoxFit.cover,
           ),
           titleSection,
           buttonSection,
           textSection
         ]
       ),
     ),
   );
  }
  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }
}//Myapp

